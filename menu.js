import { StatusBar } from "expo-status-bar";
import {StyleSheet, Text, View, ScrollView, TouchableOpacity,} from "react-native";

export default function Menu({ navigation }) {
  return (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate("CountMoviment")}>
        <Text>Count</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate("ImageMoviment")}>
        <Text>Imagem</Text>
      </TouchableOpacity>
    </View>
  );
}